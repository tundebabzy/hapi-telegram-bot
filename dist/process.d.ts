import { TelegramUpdate } from "./interface";
/**
 * Returns an array of relevant Updates that have been sorted.
 * A relevant update passes at least one of the following tests:
 * - contains a bot command. In this case, the latest Update is returned
 * - the latest Update is the last Update.
 * - Updates that are out of context are removed e.g if an edited message is received,
 * state is rewinded back to the state when the original message was received
 * consequently dumping all the updates that came after the original message.
 * @param updates Array of previous Updates
 * @param latest the latest Update received
 */
declare const transform: (updates: TelegramUpdate[], latest?: TelegramUpdate | undefined) => TelegramUpdate[];
export { transform };
//# sourceMappingURL=process.d.ts.map