import { ConfigOption } from './interface';
declare const _default: Map<string, (chatId: string | number, opts: ConfigOption) => (token: string) => Promise<import("axios").AxiosResponse<any>>>;
export default _default;
//# sourceMappingURL=methods.d.ts.map