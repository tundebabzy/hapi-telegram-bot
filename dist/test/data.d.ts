import { TelegramUpdate } from "../interface";
declare const FALSE_START_UPDATE: {
    "update_id": number;
    "message": {
        "date": number;
        "chat": {
            "last_name": string;
            "id": number;
            "type": string;
            "first_name": string;
            "username": string;
        };
        "message_id": number;
        "from": {
            "is_bot": boolean;
            "last_name": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "text": string;
    };
};
declare const INLINE_QUERY_UPDATE: {
    "update_id": number;
    "inline_query": {
        "id": string;
        "from": {
            "is_bot": boolean;
            "last_name": string;
            "type": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "query": string;
        "offset": string;
    };
};
declare const SORTED_UPDATES_1: {
    "update_id": number;
    "message": {
        "date": number;
        "chat": {
            "last_name": string;
            "id": number;
            "type": string;
            "first_name": string;
            "username": string;
        };
        "message_id": number;
        "from": {
            "is_bot": boolean;
            "last_name": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "text": string;
    };
}[];
declare const START_COMMAND: TelegramUpdate;
declare const UPDATES_INCLUDING_THOSE_WITHOUT_MESSAGE: ({
    "update_id": number;
    "message": {
        "date": number;
        "chat": {
            "last_name": string;
            "id": number;
            "type": string;
            "first_name": string;
            "username": string;
        };
        "message_id": number;
        "from": {
            "is_bot": boolean;
            "last_name": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "text": string;
    };
    "edited_message"?: undefined;
    "inline_query"?: undefined;
} | {
    "update_id": number;
    "edited_message": {
        "date": number;
        "chat": {
            "last_name": string;
            "type": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "message_id": number;
        "from": {
            "is_bot": boolean;
            "last_name": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "text": string;
        "edit_date": number;
    };
    "message"?: undefined;
    "inline_query"?: undefined;
} | {
    "update_id": number;
    "inline_query": {
        "id": string;
        "from": {
            "is_bot": boolean;
            "last_name": string;
            "type": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "query": string;
        "offset": string;
    };
    "message"?: undefined;
    "edited_message"?: undefined;
})[];
declare const UNSORTED_UPDATES_1: {
    "update_id": number;
    "message": {
        "date": number;
        "chat": {
            "last_name": string;
            "id": number;
            "type": string;
            "first_name": string;
            "username": string;
        };
        "message_id": number;
        "from": {
            "is_bot": boolean;
            "last_name": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "text": string;
    };
}[];
declare const UNSORTED_UPDATES_WITH_EDITED_MESSAGE: ({
    "update_id": number;
    "message": {
        "date": number;
        "chat": {
            "last_name": string;
            "id": number;
            "type": string;
            "first_name": string;
            "username": string;
        };
        "message_id": number;
        "from": {
            "is_bot": boolean;
            "last_name": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "text": string;
    };
    "edited_message"?: undefined;
} | {
    "update_id": number;
    "edited_message": {
        "date": number;
        "chat": {
            "last_name": string;
            "type": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "message_id": number;
        "from": {
            "is_bot": boolean;
            "last_name": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "text": string;
        "edit_date": number;
    };
    "message"?: undefined;
})[];
declare const UNSORTED_UPDATES_WITH_EDITED_MESSAGE_2: ({
    "update_id": number;
    "message": {
        "date": number;
        "chat": {
            "last_name": string;
            "id": number;
            "type": string;
            "first_name": string;
            "username": string;
        };
        "message_id": number;
        "from": {
            "is_bot": boolean;
            "last_name": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "text": string;
    };
    "edited_message"?: undefined;
} | {
    "update_id": number;
    "edited_message": {
        "date": number;
        "chat": {
            "last_name": string;
            "type": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "message_id": number;
        "from": {
            "is_bot": boolean;
            "last_name": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "text": string;
        "edit_date": number;
    };
    "message"?: undefined;
})[];
declare const UNSORTED_UPDATES_WITH_EDITED_MESSAGE_CORRECT_CONTEXT: ({
    "update_id": number;
    "message": {
        "date": number;
        "chat": {
            "last_name": string;
            "id": number;
            "type": string;
            "first_name": string;
            "username": string;
        };
        "message_id": number;
        "from": {
            "is_bot": boolean;
            "last_name": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "text": string;
    };
    "edited_message"?: undefined;
} | {
    "update_id": number;
    "edited_message": {
        "date": number;
        "chat": {
            "last_name": string;
            "type": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "message_id": number;
        "from": {
            "is_bot": boolean;
            "last_name": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "text": string;
        "edit_date": number;
    };
    "message"?: undefined;
})[];
declare const SAMPLE_INLINE_QUERY: {
    "update_id": number;
    "inline_query": {
        "id": string;
        "from": {
            "is_bot": boolean;
            "last_name": string;
            "type": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "query": string;
        "offset": string;
    };
};
declare const SAMPLE_EDITED_MESSAGE: {
    "update_id": number;
    "edited_message": {
        "date": number;
        "chat": {
            "last_name": string;
            "type": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "message_id": number;
        "from": {
            "is_bot": boolean;
            "last_name": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "text": string;
        "edit_date": number;
    };
};
declare const SAMPLE_MESSAGE: {
    "update_id": number;
    "message": {
        "date": number;
        "chat": {
            "last_name": string;
            "id": number;
            "type": string;
            "first_name": string;
            "username": string;
        };
        "message_id": number;
        "from": {
            "is_bot": boolean;
            "last_name": string;
            "id": number;
            "first_name": string;
            "username": string;
        };
        "text": string;
    };
};
export { FALSE_START_UPDATE, INLINE_QUERY_UPDATE, UNSORTED_UPDATES_1, UNSORTED_UPDATES_WITH_EDITED_MESSAGE, UNSORTED_UPDATES_WITH_EDITED_MESSAGE_2, UNSORTED_UPDATES_WITH_EDITED_MESSAGE_CORRECT_CONTEXT, UPDATES_INCLUDING_THOSE_WITHOUT_MESSAGE, SAMPLE_INLINE_QUERY, SAMPLE_EDITED_MESSAGE, SAMPLE_MESSAGE, SORTED_UPDATES_1, START_COMMAND };
//# sourceMappingURL=data.d.ts.map