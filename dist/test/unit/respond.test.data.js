"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BOT_COMMAND_MESSAGES = [
    {
        update_id: 630580959,
        message: {
            message_id: 43,
            from: {
                id: 396790711,
                is_bot: false,
                first_name: 'Babatunde',
                last_name: 'Akinyanmi',
                username: 'Tundebabzy',
                language_code: 'en'
            },
            chat: {
                id: 396790711,
                first_name: 'Babatunde',
                last_name: 'Akinyanmi',
                username: 'Tundebabzy',
                type: 'private'
            },
            date: 1580370517,
            text: '/start',
            entities: [
                {
                    offset: 0,
                    length: 6,
                    type: 'bot_command'
                }
            ]
        }
    },
    {
        update_id: 630580960,
        message: {
            message_id: 45,
            from: {
                id: 396790711,
                is_bot: false,
                first_name: 'Babatunde',
                last_name: 'Akinyanmi',
                username: 'Tundebabzy',
                language_code: 'en'
            },
            chat: {
                id: 396790711,
                first_name: 'Babatunde',
                last_name: 'Akinyanmi',
                username: 'Tundebabzy',
                type: 'private'
            },
            date: 1580374191,
            text: '1234567890',
            entities: [
                {
                    offset: 0,
                    length: 10,
                    type: 'phone_number'
                }
            ]
        }
    },
    {
        update_id: 630580961,
        message: {
            message_id: 47,
            from: {
                id: 396790711,
                is_bot: false,
                first_name: 'Babatunde',
                last_name: 'Akinyanmi',
                username: 'Tundebabzy',
                language_code: 'en'
            },
            chat: {
                id: 396790711,
                first_name: 'Babatunde',
                last_name: 'Akinyanmi',
                username: 'Tundebabzy',
                type: 'private'
            },
            date: 1580374191,
            text: 'test@example.com',
            entities: [
                {
                    offset: 0,
                    length: 16,
                    type: 'email'
                }
            ]
        }
    }
];
exports.BOT_COMMAND_MESSAGES = BOT_COMMAND_MESSAGES;
const CONFIG1 = {
    '/start': [
        {
            method: 'sendMessage',
            text: 'Message 1 received'
        },
        {
            method: 'sendMessage',
            text: '__Message 2 received__. Expecting one more message'
        },
        {
            method: 'sendMessage',
            text: 'Message 3 received. No more messages to be sent'
        }
    ]
};
exports.CONFIG1 = CONFIG1;
const ITALIC_COMMAND_UPDATE = {
    update_id: 630580958,
    message: {
        message_id: 41,
        from: {
            id: 396790711,
            is_bot: false,
            first_name: 'Babatunde',
            last_name: 'Akinyanmi',
            username: 'Tundebabzy',
            language_code: 'en'
        },
        chat: {
            id: 396790711,
            first_name: 'Babatunde',
            last_name: 'Akinyanmi',
            username: 'Tundebabzy',
            type: 'private'
        },
        date: 1580370517,
        text: '/test',
        entities: [
            {
                offset: 0,
                length: 4,
                type: 'italic'
            }
        ]
    }
};
exports.ITALIC_COMMAND_UPDATE = ITALIC_COMMAND_UPDATE;
//# sourceMappingURL=respond.test.data.js.map