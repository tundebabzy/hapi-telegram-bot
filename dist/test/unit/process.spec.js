"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
const data_1 = require("../data");
const process_1 = require("../../process");
const chai_1 = require("chai");
const rambda_1 = require("rambda");
const utils_1 = require("../../utils");
chai_1.should();
describe('process.ts', function () {
    describe('isACommand', function () {
        it('should detect that Update contains a bot command when given an Update with a bot command', function () {
            chai_1.expect(utils_1.isACommand(data_1.START_COMMAND)).to.be.true;
        });
        it('should return false for an Update that is not a bot command', function () {
            const updates = [data_1.INLINE_QUERY_UPDATE, data_1.FALSE_START_UPDATE];
            const result = rambda_1.map(utils_1.isACommand, updates);
            chai_1.expect(rambda_1.all(x => Boolean(x), result)).to.be.false;
        });
    });
    describe('transform', function () {
        it('should filter out Updates without a Message object', function () {
            const context = process_1.transform(data_1.UPDATES_INCLUDING_THOSE_WITHOUT_MESSAGE);
            chai_1.expect(rambda_1.init(context)).to.not.deep.include(data_1.INLINE_QUERY_UPDATE);
        });
        it('should return Updates sorted in ascending order of message id', function () {
            const context = process_1.transform(data_1.UNSORTED_UPDATES_1);
            context.should.deep.equal(data_1.SORTED_UPDATES_1);
        });
        it('should remove updates that are out of context', function () {
            const context1 = process_1.transform(rambda_1.init(data_1.UNSORTED_UPDATES_WITH_EDITED_MESSAGE), rambda_1.last(data_1.UNSORTED_UPDATES_WITH_EDITED_MESSAGE));
            const context2 = process_1.transform(rambda_1.init(data_1.UNSORTED_UPDATES_WITH_EDITED_MESSAGE_2), rambda_1.last(data_1.UNSORTED_UPDATES_WITH_EDITED_MESSAGE_2));
            chai_1.expect(context1).to.deep.equal([rambda_1.last(data_1.UNSORTED_UPDATES_WITH_EDITED_MESSAGE)]);
            chai_1.expect(context2).to.deep.equal(data_1.UNSORTED_UPDATES_WITH_EDITED_MESSAGE_CORRECT_CONTEXT);
        });
    });
});
//# sourceMappingURL=process.spec.js.map