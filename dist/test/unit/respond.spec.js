"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const respond_1 = require("../../respond");
const rambda_1 = require("rambda");
const respond_test_data_1 = require("./respond.test.data");
describe('respond.ts', function () {
    describe('respond', function () {
        it('should return a function', function () {
            chai_1.assert.isFunction(respond_1.respond(rambda_1.init(respond_test_data_1.BOT_COMMAND_MESSAGES), respond_test_data_1.CONFIG1, rambda_1.last(respond_test_data_1.BOT_COMMAND_MESSAGES)));
        });
        it('should return a function', function () {
            chai_1.assert.isFunction(respond_1.respond(rambda_1.init([]), respond_test_data_1.CONFIG1, {}));
        });
        it('should return a void function if there are no updates', async function () {
            const result = await respond_1.respond(rambda_1.init([]), respond_test_data_1.CONFIG1, {})('kjkkj');
            chai_1.expect(result).to.be.undefined;
        });
        it('should return a void function if there is no config', async function () {
            const result = await respond_1.respond(rambda_1.init(respond_test_data_1.BOT_COMMAND_MESSAGES), {}, rambda_1.last(respond_test_data_1.BOT_COMMAND_MESSAGES))('jhjh');
            chai_1.expect(result).to.be.undefined;
        });
        it('should throw an error if no function can be returned');
    });
});
//# sourceMappingURL=respond.spec.js.map