"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
const chai_1 = require("chai");
const utils_1 = require("../../utils");
const data_1 = require("../data");
const utils_test_data_1 = require("./utils.test.data");
chai_1.should();
describe('utils.ts', function () {
    describe('getObjectProperty', function () {
        it('returns the correct nested property of a given object using a "." as a delimiter', function () {
            const o = {
                a: {
                    b: 1
                }
            };
            chai_1.expect(utils_1.getObjectProperty(o, 'a.b')).to.equal(1);
        });
        it('returns the correct nested property of a given object using a "." as a delimiter', function () {
            const o = {
                a: {
                    b: 1,
                    c: {
                        d: {
                            e: 'Yes'
                        }
                    }
                }
            };
            chai_1.expect(utils_1.getObjectProperty(o, 'a.c.d.e')).to.equal('Yes');
        });
        it('returns the correct property of a given object using a single string', function () {
            const o = { a: 1 };
            chai_1.expect(utils_1.getObjectProperty(o, 'a')).to.equal(1);
        });
    });
    describe('isAMessage', function () {
        it('returns true is Update contains a `message` property', function () {
            chai_1.expect(utils_1.isAMessage(data_1.SAMPLE_MESSAGE)).to.be.true;
        });
        it('returns true if Update contains a `edited_message` property', function () {
            chai_1.expect(utils_1.isAMessage(data_1.SAMPLE_EDITED_MESSAGE)).to.be.true;
        });
        it('returns false if Update has a `inline_query` property', function () {
            chai_1.expect(utils_1.isAMessage(data_1.SAMPLE_INLINE_QUERY)).to.be.false;
        });
    });
    describe('getCommand', function () {
        it('should return a string', function () {
            (utils_1.getCommand(utils_test_data_1.BOT_COMMAND_UPDATE.message).should.be.a('string'));
        });
        it('should return an empty string if a Message is not a bot command', function () {
            chai_1.expect(utils_1.getCommand(utils_test_data_1.ITALIC_COMMAND_UPDATE.message)).to.equal('');
        });
        it('should return the correct string when Message is a bot command', function () {
            chai_1.expect(utils_1.getCommand(utils_test_data_1.BOT_COMMAND_UPDATE.message)).to.equal('/start');
        });
    });
});
//# sourceMappingURL=utils.spec.js.map