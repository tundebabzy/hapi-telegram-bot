"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const FALSE_START_UPDATE = {
    "update_id": 10000,
    "message": {
        "date": 1441645532,
        "chat": {
            "last_name": "Test Lastname",
            "id": 1111111,
            "type": "private",
            "first_name": "Test Firstname",
            "username": "Testusername"
        },
        "message_id": 1369,
        "from": {
            "is_bot": false,
            "last_name": "Test Lastname",
            "id": 1111111,
            "first_name": "Test Firstname",
            "username": "Testusername"
        },
        "text": "/start"
    }
};
exports.FALSE_START_UPDATE = FALSE_START_UPDATE;
const INLINE_QUERY_UPDATE = {
    "update_id": 10000,
    "inline_query": {
        "id": "134567890097",
        "from": {
            "is_bot": false,
            "last_name": "Test Lastname",
            "type": "private",
            "id": 1111111,
            "first_name": "Test Firstname",
            "username": "Testusername"
        },
        "query": "inline query",
        "offset": ""
    }
};
exports.INLINE_QUERY_UPDATE = INLINE_QUERY_UPDATE;
const SORTED_UPDATES_1 = [{
        "update_id": 10001,
        "message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "id": 1111111,
                "type": "private",
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1365,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "/start"
        }
    }, {
        "update_id": 10002,
        "message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "id": 1111111,
                "type": "private",
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1367,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "/start"
        }
    }, {
        "update_id": 10000,
        "message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "id": 1111111,
                "type": "private",
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1369,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "/start"
        }
    }];
exports.SORTED_UPDATES_1 = SORTED_UPDATES_1;
const START_COMMAND = {
    "update_id": 10000,
    "message": {
        "date": 1441645532,
        "chat": {
            "last_name": "Test Lastname",
            "type": "private",
            "id": 1111111,
            "first_name": "Test Firstname",
            "username": "Testusername"
        },
        "message_id": 1365,
        "from": {
            "is_bot": false,
            "last_name": "Test Lastname",
            "id": 1111111,
            "first_name": "Test Firstname",
            "username": "Testusername"
        },
        "text": "Bold and italics",
        "entities": [
            {
                "type": "bot_command",
                "offset": 0,
                "length": 6
            }
        ]
    }
};
exports.START_COMMAND = START_COMMAND;
const UPDATES_INCLUDING_THOSE_WITHOUT_MESSAGE = [{
        "update_id": 10000,
        "message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "id": 1111111,
                "type": "private",
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1369,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "/start"
        }
    }, {
        "update_id": 10001,
        "message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "id": 1111111,
                "type": "private",
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1365,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "/start"
        }
    }, {
        "update_id": 10002,
        "edited_message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "type": "private",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1369,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "Edited text",
            "edit_date": 1441646600
        }
    }, {
        "update_id": 10003,
        "inline_query": {
            "id": "134567890097",
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "type": "private",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "query": "inline query",
            "offset": ""
        }
    }];
exports.UPDATES_INCLUDING_THOSE_WITHOUT_MESSAGE = UPDATES_INCLUDING_THOSE_WITHOUT_MESSAGE;
const UNSORTED_UPDATES_1 = [{
        "update_id": 10000,
        "message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "id": 1111111,
                "type": "private",
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1369,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "/start"
        }
    }, {
        "update_id": 10001,
        "message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "id": 1111111,
                "type": "private",
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1365,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "/start"
        }
    }, {
        "update_id": 10002,
        "message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "id": 1111111,
                "type": "private",
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1367,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "/start"
        }
    }];
exports.UNSORTED_UPDATES_1 = UNSORTED_UPDATES_1;
const UNSORTED_UPDATES_WITH_EDITED_MESSAGE = [{
        "update_id": 10000,
        "message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "id": 1111111,
                "type": "private",
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1369,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "/start"
        }
    }, {
        "update_id": 10001,
        "message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "id": 1111111,
                "type": "private",
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1365,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "/start"
        }
    }, {
        "update_id": 10002,
        "message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "id": 1111111,
                "type": "private",
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1367,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "/start"
        }
    }, {
        "update_id": 10003,
        "edited_message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "type": "private",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1365,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "Edited text",
            "edit_date": 1441646600
        }
    }];
exports.UNSORTED_UPDATES_WITH_EDITED_MESSAGE = UNSORTED_UPDATES_WITH_EDITED_MESSAGE;
const UNSORTED_UPDATES_WITH_EDITED_MESSAGE_2 = [{
        "update_id": 10000,
        "message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "id": 1111111,
                "type": "private",
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1369,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "/start"
        }
    }, {
        "update_id": 10001,
        "message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "id": 1111111,
                "type": "private",
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1365,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "/start"
        }
    }, {
        "update_id": 10002,
        "message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "id": 1111111,
                "type": "private",
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1367,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "/start"
        }
    }, {
        "update_id": 10003,
        "edited_message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "type": "private",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1367,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "Edited text",
            "edit_date": 1441646600
        }
    }];
exports.UNSORTED_UPDATES_WITH_EDITED_MESSAGE_2 = UNSORTED_UPDATES_WITH_EDITED_MESSAGE_2;
const UNSORTED_UPDATES_WITH_EDITED_MESSAGE_CORRECT_CONTEXT = [{
        "update_id": 10001,
        "message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "id": 1111111,
                "type": "private",
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1365,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "/start"
        }
    }, {
        "update_id": 10003,
        "edited_message": {
            "date": 1441645532,
            "chat": {
                "last_name": "Test Lastname",
                "type": "private",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "message_id": 1367,
            "from": {
                "is_bot": false,
                "last_name": "Test Lastname",
                "id": 1111111,
                "first_name": "Test Firstname",
                "username": "Testusername"
            },
            "text": "Edited text",
            "edit_date": 1441646600
        }
    }];
exports.UNSORTED_UPDATES_WITH_EDITED_MESSAGE_CORRECT_CONTEXT = UNSORTED_UPDATES_WITH_EDITED_MESSAGE_CORRECT_CONTEXT;
// ============================================================================
const SAMPLE_INLINE_QUERY = {
    "update_id": 10003,
    "inline_query": {
        "id": "134567890097",
        "from": {
            "is_bot": false,
            "last_name": "Test Lastname",
            "type": "private",
            "id": 1111111,
            "first_name": "Test Firstname",
            "username": "Testusername"
        },
        "query": "inline query",
        "offset": ""
    }
};
exports.SAMPLE_INLINE_QUERY = SAMPLE_INLINE_QUERY;
const SAMPLE_EDITED_MESSAGE = {
    "update_id": 10002,
    "edited_message": {
        "date": 1441645532,
        "chat": {
            "last_name": "Test Lastname",
            "type": "private",
            "id": 1111111,
            "first_name": "Test Firstname",
            "username": "Testusername"
        },
        "message_id": 1369,
        "from": {
            "is_bot": false,
            "last_name": "Test Lastname",
            "id": 1111111,
            "first_name": "Test Firstname",
            "username": "Testusername"
        },
        "text": "Edited text",
        "edit_date": 1441646600
    }
};
exports.SAMPLE_EDITED_MESSAGE = SAMPLE_EDITED_MESSAGE;
const SAMPLE_MESSAGE = {
    "update_id": 10000,
    "message": {
        "date": 1441645532,
        "chat": {
            "last_name": "Test Lastname",
            "id": 1111111,
            "type": "private",
            "first_name": "Test Firstname",
            "username": "Testusername"
        },
        "message_id": 1369,
        "from": {
            "is_bot": false,
            "last_name": "Test Lastname",
            "id": 1111111,
            "first_name": "Test Firstname",
            "username": "Testusername"
        },
        "text": "/start"
    }
};
exports.SAMPLE_MESSAGE = SAMPLE_MESSAGE;
//# sourceMappingURL=data.js.map