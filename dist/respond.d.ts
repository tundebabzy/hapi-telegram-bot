import { TelegramUpdate, Config } from "./interface";
import { AxiosResponse } from "axios";
/**
 * Returns a function that performs a Telegram method.
 * @see {@link https://core.telegram.org/bots/api#available-methods} for telegram methods
 * @param updates Array of Updates
 * @param config Config object
 */
declare const respond: (updates: TelegramUpdate[], config: Config, latest: TelegramUpdate) => (token: string) => Promise<AxiosResponse<any>> | Promise<undefined>;
export { respond };
//# sourceMappingURL=respond.d.ts.map