"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("./constants");
const utils_1 = require("./utils");
const respond_1 = require("./respond");
// temporary data structure to mimic our datastore. This should do the work
// of redis
const updates = [];
const plugin = {
    pkg: require('../package.json'),
    register: async function (server, options) {
        const { config, secretPath, token } = options;
        const serverPath = utils_1.getServerPath(token, secretPath);
        const secretUrl = `https://francisakindele.com${serverPath}`;
        const telegramApiPath = `${constants_1.TELEGRAM_ENDPOINT}${token}`;
        server.route({
            path: serverPath,
            method: 'POST',
            handler: async function (request, h) {
                const payload = request.payload;
                if (utils_1.isDuplicate(updates, payload)) {
                    return 'Thanks';
                }
                else if (utils_1.isAMessage(payload)) {
                    const fn = respond_1.respond(updates, config, payload);
                    console.log(token);
                    const response = await fn(token);
                    console.log(response);
                    return 'ok';
                }
            }
        });
        utils_1.registerWebhookAsync(telegramApiPath, secretUrl).catch(e => console.log(e));
    }
};
exports.plugin = plugin;
//# sourceMappingURL=index.js.map