"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const rambda_1 = require("rambda");
const getCommand = (message) => {
    if (message.entities) {
        const command = rambda_1.find((a) => a.type === 'bot_command', message.entities);
        if (command && message.text) {
            return rambda_1.slice(command.offset, rambda_1.add(command.offset, command.length), message.text);
        }
    }
    return '';
};
exports.getCommand = getCommand;
/**
 * Returns the value of the given property name
 * @param seed An object
 * @param propertyName Name of object property. Where the property name is nested, use a "." where it is a nested property
 */
const getObjectProperty = (seed, propertyName) => {
    return rambda_1.reduce(_getValue, seed, propertyName.split('.'));
};
exports.getObjectProperty = getObjectProperty;
const _getValue = (seed, propertyName) => {
    if (!propertyName) {
        throw new Error('null or undefined value supplier as an argument');
    }
    return typeof seed === 'undefined' ? undefined : seed[propertyName];
};
/**
 * helper function that returns a server path which defaults to the telegram api token
 * @param path user defined server path
 * @param token telegram api token
 */
const getServerPath = (token, path) => {
    if (!path && !token) {
        throw "No token found for this server";
    }
    return `/${path || token}`;
};
exports.getServerPath = getServerPath;
/**
 * Returns true if the given Update has a message that is a bot_command.
 * @param message TelegramMessage
 */
const isACommand = (update) => {
    return Boolean(update.message && update.message.entities && update.message.entities.find(item => item.type === 'bot_command'));
};
exports.isACommand = isACommand;
/**
 * Returns true if the given Update is a message
 * @param update an Update
 */
const isAMessage = (update) => {
    return Boolean(update.message || update.edited_message);
};
exports.isAMessage = isAMessage;
/**
 * Return true if the latest update is already known to the server.
 * @param previousUpdates An array containing previously stored updates
 * @param latestUpdate The latest update received by the server
 */
const isDuplicate = (previousUpdates, latestUpdate) => {
    return Boolean(previousUpdates.find(item => item.update_id === latestUpdate.update_id));
};
exports.isDuplicate = isDuplicate;
/**
 * Registers a webhook endpoint with Telegram.
 * @async
 * @param telegramAddress Telegram address for registering webhooks. This should be in the form https://api.telegram.org/bot<token>. Note that Telegram only accepts https
 * @param secretEndpoint URL where Telegram should send Updates to
 */
const registerWebhookAsync = (telegramAddress, secretEndpoint) => {
    return axios_1.default.post(`${telegramAddress}/setWebhook`, {
        url: secretEndpoint,
    });
};
exports.registerWebhookAsync = registerWebhookAsync;
//# sourceMappingURL=utils.js.map