"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const constants_1 = require("../constants");
function default_1(chatId) {
    const methodName = 'sendMessage';
    return (token) => {
        const endpoint = `${constants_1.TELEGRAM_ENDPOINT}${token}/${methodName}`;
        return axios_1.default.post(endpoint, {
            chat_id: chatId,
            text: "I am embarrased that this is happening but I seem to have lost track of our conversation. Please repeat your last message or let's start over with a new command"
        });
    };
}
exports.default = default_1;
;
//# sourceMappingURL=default-reply.js.map