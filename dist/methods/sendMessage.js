"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const constants_1 = require("../constants");
const rambda_1 = require("rambda");
const sendMessage = function (chatId, opts) {
    const methodName = 'sendMessage';
    return (token) => {
        const endpoint = `${constants_1.TELEGRAM_ENDPOINT}${token}/${methodName}`;
        const data = {
            chat_id: chatId,
            text: opts.text
        };
        const transformToKeyboard = (x) => {
            return ({
                text: x.label,
                request_contact: x.request_contact,
                request_location: x.request_location,
                request_poll: x.request_poll
            });
        };
        const markup = (config) => {
            if (!config.keyboards) {
                config.keyboards = [];
            }
            return {
                keyboard: [rambda_1.map(transformToKeyboard, config.keyboards)]
            };
        };
        if (opts.keyboards && opts.keyboards.length) {
            data.reply_markup = markup(opts);
        }
        return axios_1.default.post(endpoint, data);
    };
};
exports.sendMessage = sendMessage;
//# sourceMappingURL=sendMessage.js.map