"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sendMessage_1 = require("./sendMessage");
exports.sendMessage = sendMessage_1.sendMessage;
const default_reply_1 = __importDefault(require("./default-reply"));
exports.defaultReply = default_reply_1.default;
//# sourceMappingURL=index.js.map