import { ConfigOption } from "../interface";
declare const sendMessage: (chatId: string | number, opts: ConfigOption) => (token: string) => Promise<import("axios").AxiosResponse<any>>;
export { sendMessage };
//# sourceMappingURL=sendMessage.d.ts.map