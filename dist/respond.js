"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const rambda_1 = require("rambda");
const utils_1 = require("./utils");
const methods_1 = __importDefault(require("./methods"));
const moment_1 = require("moment");
const moment = require("moment");
const commandInFront = (updates) => {
    const commandIndex = rambda_1.findIndex(utils_1.isACommand, updates);
    return commandIndex !== -1 ? rambda_1.slice(commandIndex, updates.length, updates) : [];
};
const getHandler = (pair) => {
    if (pair) {
        const [message, option] = pair;
        if (option && message) {
            const fn = methods_1.default.get(option.method);
            if (fn) {
                return fn(message.chat.id, option);
            }
        }
        else if (message.chat.id) {
            const defaultReply = methods_1.default.get('default-reply');
            if (defaultReply) {
                return defaultReply(message.chat.id, option);
            }
        }
    }
    return async (...a) => undefined;
};
const getUpdateTime = (update) => {
    if (update.message) {
        return moment_1.unix(update.message.date);
    }
    else if (update.edited_message && update.edited_message.edit_date) {
        return moment_1.unix(update.edited_message.edit_date);
    }
    return moment(1580370517);
};
/**
 * Returns a function that performs a Telegram method.
 * @see {@link https://core.telegram.org/bots/api#available-methods} for telegram methods
 * @param updates Array of Updates
 * @param config Config object
 */
const respond = (updates, config, latest) => {
    const respondFn = rambda_1.pipe(transform, rambda_1.partial(zipToConfig, config), rambda_1.partial(rambda_1.takeLast, 1), rambda_1.partial(rambda_1.last), rambda_1.partial(getHandler));
    return respondFn(updates, latest);
};
exports.respond = respond;
const transform = (updates, latest) => {
    const joinedUpdates = rambda_1.concat(updates, [latest]);
    return rambda_1.pipe(transformUpdates, transformEditedMessage)(joinedUpdates);
};
const _transformToMessage = rambda_1.ifElse((update) => Boolean(update && update.message && Object.keys(update.message).length > 0), (update) => update.message, (update) => update && update.edited_message && Object.keys(update.edited_message).length ? update.edited_message : {});
const _messageReducer = (accumulator, current) => {
    const lastInAccumulator = rambda_1.last(accumulator);
    if (lastInAccumulator && lastInAccumulator.message_id === current.message_id) {
        return rambda_1.adjust(accumulator.length - 1, (item) => rambda_1.merge(item, current), accumulator);
    }
    return rambda_1.append(current, accumulator);
};
/**
 * Returns an array of `Message`s after merging edited messages with their original messages
 * where necessary
 * @param updates Array of Updates
 */
const transformEditedMessage = (updates) => {
    // all things being equal we should only need the last 2 elements since the updates are already
    // sorted with the latest update at the end.
    return rambda_1.reduce(_messageReducer, [], rambda_1.map((item) => _transformToMessage(item), updates));
};
/**
 * Returns an array of Updates after sorting them by date of receipt and filtering out messages that were
 * created after the newest received Update.
 * @param updates Array of Updates
 */
const transformUpdates = (updates) => {
    const sortUpdatesByDate = (x, y) => {
        const a = getUpdateTime(x);
        const b = getUpdateTime(y);
        if (a < b) {
            return -1;
        }
        else if (a > b) {
            return 1;
        }
        else {
            return 0;
        }
    };
    const notLaterThanLatest = (latest) => (update) => {
        return latest ? getUpdateTime(update) <= getUpdateTime(latest) : false;
    };
    return commandInFront(rambda_1.filter(notLaterThanLatest(rambda_1.last(updates)), rambda_1.sort(sortUpdatesByDate, updates)));
};
const zipToConfig = (config, messages) => {
    const message = rambda_1.head(messages);
    if (messages && messages.length && message && config && config[utils_1.getCommand(message)]) {
        return rambda_1.zip(messages, config[utils_1.getCommand(message)]);
    }
    return [];
};
//# sourceMappingURL=respond.js.map