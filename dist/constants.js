"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TELEGRAM_ENDPOINT = 'https://api.telegram.org/bot';
exports.TELEGRAM_ENDPOINT = TELEGRAM_ENDPOINT;
const UPDATES = [
    'message', 'edited_message', 'channel_post', 'edited_channel_post',
    'inline_query', 'chosen_inline_result', 'callback_query',
    'shipping_query', 'pre_checkout_query', 'poll'
];
exports.UPDATES = UPDATES;
//# sourceMappingURL=constants.js.map