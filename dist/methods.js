"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const methods = __importStar(require("./methods/index"));
exports.default = new Map([
    ['default-reply', (chatId, opts) => methods.defaultReply(chatId)],
    ['sendMessage', (chatId, opts) => methods.sendMessage(chatId, opts)]
]);
//# sourceMappingURL=methods.js.map