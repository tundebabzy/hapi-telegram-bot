import { TelegramUpdate, TelegramMessage } from "./interface";
interface AnyDict {
    [key: string]: any;
}
declare const getCommand: (message: TelegramMessage) => string;
/**
 * Returns the value of the given property name
 * @param seed An object
 * @param propertyName Name of object property. Where the property name is nested, use a "." where it is a nested property
 */
declare const getObjectProperty: (seed: AnyDict, propertyName: string) => any;
/**
 * helper function that returns a server path which defaults to the telegram api token
 * @param path user defined server path
 * @param token telegram api token
 */
declare const getServerPath: (token: string, path?: string) => string;
/**
 * Returns true if the given Update has a message that is a bot_command.
 * @param message TelegramMessage
 */
declare const isACommand: (update: TelegramUpdate) => boolean;
/**
 * Returns true if the given Update is a message
 * @param update an Update
 */
declare const isAMessage: (update: TelegramUpdate) => Boolean;
/**
 * Return true if the latest update is already known to the server.
 * @param previousUpdates An array containing previously stored updates
 * @param latestUpdate The latest update received by the server
 */
declare const isDuplicate: (previousUpdates: TelegramUpdate[], latestUpdate: TelegramUpdate) => boolean;
/**
 * Registers a webhook endpoint with Telegram.
 * @async
 * @param telegramAddress Telegram address for registering webhooks. This should be in the form https://api.telegram.org/bot<token>. Note that Telegram only accepts https
 * @param secretEndpoint URL where Telegram should send Updates to
 */
declare const registerWebhookAsync: (telegramAddress: string, secretEndpoint: string) => Promise<import("axios").AxiosResponse<any>>;
export { getCommand, getObjectProperty, getServerPath, isACommand, isAMessage, isDuplicate, registerWebhookAsync };
//# sourceMappingURL=utils.d.ts.map