import { TelegramUpdate, Config, TelegramMessage, ConfigOption } from "./interface";
import { zip, head, pipe, concat, sort, reduce, filter, partial, last, map, ifElse, takeLast, adjust, merge, append, findIndex, slice, KeyValuePair, all, curry } from "rambda";
import { isACommand, getCommand } from "./utils";
import handlers from './methods';
import { Moment, unix } from "moment";
import moment = require("moment");
import { AxiosResponse } from "axios";

const commandInFront = (updates: TelegramUpdate[]) => {
    const commandIndex = findIndex(isACommand, updates)
    return commandIndex !== -1 ? slice(commandIndex, updates.length, updates) : [];
}

const getHandler = (pair: KeyValuePair<TelegramMessage, ConfigOption>): (token: string) => Promise<AxiosResponse<any>> | Promise<undefined> => {
    if (pair) {
        const [message, option] = pair;
        if (option && message) {
            const fn = handlers.get(option.method);
            if (fn) {
                return fn(message.chat.id, option);
            }
        } else if (message.chat.id) {
            const defaultReply = handlers.get('default-reply');
            if (defaultReply) {
                return defaultReply(message.chat.id, option);
            }
        }
    }
    return async (...a: any[]) => undefined;
}

const getUpdateTime = (update: TelegramUpdate): Moment => {
    if (update.message) {
        return unix(update.message.date);
    } else if (update.edited_message && update.edited_message.edit_date) {
        return unix(update.edited_message.edit_date);
    }
    return moment(1580370517);
}

/**
 * Returns a function that performs a Telegram method.
 * @see {@link https://core.telegram.org/bots/api#available-methods} for telegram methods
 * @param updates Array of Updates
 * @param config Config object
 */
const respond = (updates: TelegramUpdate[], config: Config, latest: TelegramUpdate) => {
    const respondFn = pipe(
        transform,
        partial(zipToConfig, config),
        partial(takeLast, 1),
        partial(last),
        partial(getHandler)
    );
    return respondFn(updates, latest);
}

const transform = (updates: TelegramUpdate[], latest: TelegramUpdate): TelegramMessage[] => {
    const joinedUpdates = concat(updates, [latest])
    return pipe(transformUpdates, transformEditedMessage)(joinedUpdates);
}

const _transformToMessage = ifElse(
    (update: TelegramUpdate | undefined): boolean => Boolean(update && update.message && Object.keys(update.message).length > 0),
    (update: TelegramUpdate) => update.message,
    (update: TelegramUpdate) => update && update.edited_message && Object.keys(update.edited_message).length ? update.edited_message : {}
);

const _messageReducer = (accumulator: TelegramMessage[], current: TelegramMessage) => {
    const lastInAccumulator = last(accumulator);
    if (lastInAccumulator && lastInAccumulator.message_id === current.message_id) {
        return adjust(accumulator.length - 1, (item: object) => merge(item, current), accumulator);
    }

    return append(current, accumulator);
};

/**
 * Returns an array of `Message`s after merging edited messages with their original messages
 * where necessary
 * @param updates Array of Updates
 */
const transformEditedMessage = (updates: TelegramUpdate[]) => {
    // all things being equal we should only need the last 2 elements since the updates are already
    // sorted with the latest update at the end.
    return reduce(_messageReducer, [], map((item) => _transformToMessage(item), updates));
}

/**
 * Returns an array of Updates after sorting them by date of receipt and filtering out messages that were
 * created after the newest received Update.
 * @param updates Array of Updates
 */
const transformUpdates = (updates: TelegramUpdate[]) => {
    const sortUpdatesByDate = (x: TelegramUpdate, y: TelegramUpdate) => {
        const a = getUpdateTime(x);
        const b = getUpdateTime(y);
        if (a < b) {
            return -1;
        } else if (a > b) {
            return 1;
        } else {
            return 0;
        }
    };
    const notLaterThanLatest = (latest: TelegramUpdate | undefined) => (update: TelegramUpdate): boolean => {
        return latest ? getUpdateTime(update) <= getUpdateTime(latest) : false;
    };
    return commandInFront(
        filter(
            notLaterThanLatest(last(updates)),
            sort(sortUpdatesByDate, updates)
        )
    );
};

const zipToConfig = (config: Config, messages: TelegramMessage[]) => {
    const message = head(messages);
    if (messages && messages.length && message && config && config[getCommand(message)]) {
        return zip(messages, config[getCommand(message)])
    }
    return [];
};

export {
    respond
}