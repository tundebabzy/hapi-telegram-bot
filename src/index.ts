import { Plugin, Server, Request, ResponseToolkit } from "@hapi/hapi";
import { PluginOptions, TelegramUpdate } from "./interface";
import { TELEGRAM_ENDPOINT } from "./constants";
import { getServerPath, isDuplicate, registerWebhookAsync, isAMessage } from "./utils";
import { respond } from "./respond";

// temporary data structure to mimic our datastore. This should do the work
// of redis
const updates: TelegramUpdate[] = [];

const plugin: Plugin<PluginOptions> = {
    pkg: require('../package.json'),
    register: async function (server: Server, options: PluginOptions) {
        const { config, secretPath, token } = options;
        const serverPath = getServerPath(token, secretPath);
        const secretUrl = `https://francisakindele.com${serverPath}`;
        const telegramApiPath = `${TELEGRAM_ENDPOINT}${token}`;

        server.route({
            path: serverPath,
            method: 'POST',
            handler: async function (request: Request, h: ResponseToolkit) {
                const payload: TelegramUpdate = <TelegramUpdate>request.payload;
                if (isDuplicate(updates, payload)) {
                    return 'Thanks'
                } else if (isAMessage(payload)) {
                    const fn = respond(updates, config, payload);
                    console.log(token);
                    const response = await fn(token);
                    console.log(response)
                    return 'ok';
                }
            }
        });

        registerWebhookAsync(telegramApiPath, secretUrl).catch(e => console.log(e));
    }
}

export { plugin }