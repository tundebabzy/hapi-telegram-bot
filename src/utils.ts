import { TelegramUpdate, TelegramMessageEntity, TelegramMessage } from "./interface";
import axios from "axios";
import { pipe, find, reduce, head, slice, add } from "rambda";

interface AnyDict {
    [key: string]: any
}

const getCommand = (message: TelegramMessage) => {
    if (message.entities) {
        const command = find(
            (a: TelegramMessageEntity) => a.type === 'bot_command',
            message.entities
        );
        if (command && message.text) {
            return slice(command.offset, add(command.offset, command.length), message.text);
        }
    }
    return '';
}

/**
 * Returns the value of the given property name
 * @param seed An object
 * @param propertyName Name of object property. Where the property name is nested, use a "." where it is a nested property
 */
const getObjectProperty = (seed: AnyDict, propertyName: string): any => {
    return reduce(_getValue, seed, propertyName.split('.'));
}

const _getValue = (seed: AnyDict, propertyName: string) => {
    if (!propertyName) {
        throw new Error('null or undefined value supplier as an argument');
    }
    return typeof seed === 'undefined' ? undefined : seed[propertyName];
}

/**
 * helper function that returns a server path which defaults to the telegram api token
 * @param path user defined server path
 * @param token telegram api token
 */
const getServerPath: (token: string, path?: string) => string = (token: string, path?: string) => {
    if (!path && !token) {
        throw "No token found for this server";
    }
    return `/${path || token}`;
}


/**
 * Returns true if the given Update has a message that is a bot_command.
 * @param message TelegramMessage
 */
const isACommand = (update: TelegramUpdate): boolean => {

    return Boolean(update.message && update.message.entities && update.message.entities.find(item => item.type === 'bot_command'));
};

/**
 * Returns true if the given Update is a message
 * @param update an Update
 */
const isAMessage = (update: TelegramUpdate): Boolean => {
    return Boolean(update.message || update.edited_message);
}


/**
 * Return true if the latest update is already known to the server.
 * @param previousUpdates An array containing previously stored updates
 * @param latestUpdate The latest update received by the server
 */
const isDuplicate = (previousUpdates: TelegramUpdate[], latestUpdate: TelegramUpdate) => {
    return Boolean(previousUpdates.find(item => item.update_id === latestUpdate.update_id));
};

/**
 * Registers a webhook endpoint with Telegram.
 * @async
 * @param telegramAddress Telegram address for registering webhooks. This should be in the form https://api.telegram.org/bot<token>. Note that Telegram only accepts https
 * @param secretEndpoint URL where Telegram should send Updates to
 */
const registerWebhookAsync = (telegramAddress: string, secretEndpoint: string) => {
    return axios.post(`${telegramAddress}/setWebhook`, {
        url: secretEndpoint,
    });
};

export {
    getCommand,
    getObjectProperty,
    getServerPath,
    isACommand,
    isAMessage,
    isDuplicate,
    registerWebhookAsync
}