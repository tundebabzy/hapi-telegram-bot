import mocha from 'mocha';
import { expect, assert } from "chai";
import { AssertionError } from 'assert';
import { respond } from '../../respond';
import { init, last } from 'rambda';
import { BOT_COMMAND_MESSAGES, CONFIG1 } from './respond.test.data';
import { TelegramMessage, TelegramUpdate } from '../../interface';

describe('respond.ts', function () {
    describe('respond', function () {
        it('should return a function', function () {
            assert.isFunction(respond(init(BOT_COMMAND_MESSAGES), CONFIG1, last(BOT_COMMAND_MESSAGES) as TelegramUpdate));
        });
        it('should return a function', function () {
            assert.isFunction(respond(init([]), CONFIG1, {} as TelegramUpdate));
        });
        it('should return a void function if there are no updates', async function () {
            const result = await respond(init([]), CONFIG1, {} as TelegramUpdate)('kjkkj')
            expect(result).to.be.undefined;
        });
        it('should return a void function if there is no config', async function () {
            const result = await respond(init(BOT_COMMAND_MESSAGES), {}, last(BOT_COMMAND_MESSAGES) as TelegramUpdate)('jhjh')
            expect(result).to.be.undefined;
        });
        it('should throw an error if no function can be returned');
    })
})