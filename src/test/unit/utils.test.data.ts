import { TelegramUpdate } from "../../interface";

const BOT_COMMAND_UPDATE: TelegramUpdate = {
    update_id: 630580954,
    message: {
        message_id: 33,
        from: {
            id: 396790711,
            is_bot: false,
            first_name: 'Babatunde',
            last_name: 'Akinyanmi',
            username: 'Tundebabzy',
            language_code: 'en'
        },
        chat: {
            id: 396790711,
            first_name: 'Babatunde',
            last_name: 'Akinyanmi',
            username: 'Tundebabzy',
            type: 'private'
        },
        date: 1580370517,
        text: '/start',
        entities: [
            {
                offset: 0,
                length: 6,
                type: 'bot_command'
            }
        ]
    }
};

const ITALIC_COMMAND_UPDATE: TelegramUpdate = {
    update_id: 630580958,
    message: {
        message_id: 41,
        from: {
            id: 396790711,
            is_bot: false,
            first_name: 'Babatunde',
            last_name: 'Akinyanmi',
            username: 'Tundebabzy',
            language_code: 'en'
        },
        chat: {
            id: 396790711,
            first_name: 'Babatunde',
            last_name: 'Akinyanmi',
            username: 'Tundebabzy',
            type: 'private'
        },
        date: 1580370517,
        text: '/test',
        entities: [
            {
                offset: 0,
                length: 4,
                type: 'italic'
            }
        ]
    }
};

export {
    BOT_COMMAND_UPDATE,
    ITALIC_COMMAND_UPDATE
}