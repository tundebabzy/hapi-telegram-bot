import 'mocha';
import { expect, should } from "chai";
import { getObjectProperty, isAMessage, getCommand } from '../../utils';
import { SAMPLE_INLINE_QUERY, SAMPLE_MESSAGE, SAMPLE_EDITED_MESSAGE } from '../data';
import { BOT_COMMAND_UPDATE, ITALIC_COMMAND_UPDATE } from './utils.test.data';
import { TelegramMessage } from '../../interface';

should();

describe('utils.ts', function () {
    describe('getObjectProperty', function () {
        it('returns the correct nested property of a given object using a "." as a delimiter', function () {
            const o = {
                a: {
                    b: 1
                }
            };

            expect(getObjectProperty(o, 'a.b')).to.equal(1);
        });

        it('returns the correct nested property of a given object using a "." as a delimiter', function () {
            const o = {
                a: {
                    b: 1,
                    c: {
                        d: {
                            e: 'Yes'
                        }
                    }
                }
            };

            expect(getObjectProperty(o, 'a.c.d.e')).to.equal('Yes');
        });

        it('returns the correct property of a given object using a single string', function () {
            const o = { a: 1 };
            expect(getObjectProperty(o, 'a')).to.equal(1);
        });
    });

    describe('isAMessage', function () {
        it('returns true is Update contains a `message` property', function () {
            expect(isAMessage(SAMPLE_MESSAGE)).to.be.true;
        });
        it('returns true if Update contains a `edited_message` property', function () {
            expect(isAMessage(SAMPLE_EDITED_MESSAGE)).to.be.true;
        });
        it('returns false if Update has a `inline_query` property', function () {
            expect(isAMessage(SAMPLE_INLINE_QUERY)).to.be.false;
        });
    });

    describe('getCommand', function () {
        it('should return a string', function () {
            (getCommand(BOT_COMMAND_UPDATE.message as TelegramMessage).should.be.a('string'));
        });
        it('should return an empty string if a Message is not a bot command', function () {
            expect(getCommand(ITALIC_COMMAND_UPDATE.message as TelegramMessage)).to.equal('');
        });
        it('should return the correct string when Message is a bot command', function () {
            expect(getCommand(BOT_COMMAND_UPDATE.message as TelegramMessage)).to.equal('/start');
        })
    })
});