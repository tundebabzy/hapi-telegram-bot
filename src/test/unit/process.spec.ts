import 'mocha'
import { UNSORTED_UPDATES_1, SORTED_UPDATES_1, UPDATES_INCLUDING_THOSE_WITHOUT_MESSAGE, INLINE_QUERY_UPDATE, START_COMMAND, FALSE_START_UPDATE, UNSORTED_UPDATES_WITH_EDITED_MESSAGE, UNSORTED_UPDATES_WITH_EDITED_MESSAGE_CORRECT_CONTEXT, UNSORTED_UPDATES_WITH_EDITED_MESSAGE_2 } from '../data';
import { transform } from '../../process';
import { should, expect } from "chai";
import { init, map, all, last } from 'rambda';
import { isACommand } from '../../utils';

should();

describe('process.ts', function () {
    describe('isACommand', function () {
        it('should detect that Update contains a bot command when given an Update with a bot command', function () {
            expect(isACommand(START_COMMAND)).to.be.true;
        });
        it('should return false for an Update that is not a bot command', function () {
            const updates = [INLINE_QUERY_UPDATE, FALSE_START_UPDATE];
            const result = map(isACommand, updates)
            expect(all(x => Boolean(x), result)).to.be.false;
        })
    });

    describe('transform', function () {
        it('should filter out Updates without a Message object', function () {
            const context = transform(UPDATES_INCLUDING_THOSE_WITHOUT_MESSAGE);
            expect(init(context)).to.not.deep.include(INLINE_QUERY_UPDATE);
        });

        it('should return Updates sorted in ascending order of message id', function () {
            const context = transform(UNSORTED_UPDATES_1);
            context.should.deep.equal(SORTED_UPDATES_1);
        });

        it('should remove updates that are out of context', function () {
            const context1 = transform(init(UNSORTED_UPDATES_WITH_EDITED_MESSAGE), last(UNSORTED_UPDATES_WITH_EDITED_MESSAGE));
            const context2 = transform(init(UNSORTED_UPDATES_WITH_EDITED_MESSAGE_2), last(UNSORTED_UPDATES_WITH_EDITED_MESSAGE_2));
            expect(context1).to.deep.equal([last(UNSORTED_UPDATES_WITH_EDITED_MESSAGE)]);
            expect(context2).to.deep.equal(UNSORTED_UPDATES_WITH_EDITED_MESSAGE_CORRECT_CONTEXT);
        });
    })
})