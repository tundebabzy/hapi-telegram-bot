import { TelegramUpdate } from "./interface";
import { pipe, append, sort, filter, curry } from "rambda";
import { isACommand } from "./utils";


/**
 * This is a sort function for sorting in ascending order of `Message.message_id`
 * @param a Update
 * @param b Update
 */
const _messageInAscendingOrder = (a: TelegramUpdate, b: TelegramUpdate) => {
    const aObj = a.message || a.edited_message;
    const bObj = b.message || b.edited_message;

    if (aObj && aObj.message_id && bObj && bObj.message_id) {
        return aObj.message_id - bObj.message_id;
    } else {
        return 0;
    }
};

/**
 * Returns an array of Updates after performing the following operations
 * - remove every Update that does not have a Message
 * - sort the Updates in acceptable order.
 * - remove Updates that are out of context. This will apply when the latest
 * - add the latest Update to the queue
 * message is an edited message.
 * @param updates An array of Updates
 * @param latest An Update representing the latest Update received
 */
const _updateQueueTransform = (updates: TelegramUpdate[], latest?: TelegramUpdate): TelegramUpdate[] => {
    const messageIsDefined = (update: TelegramUpdate) => Boolean(update.message || update.edited_message);
    const notOutOfContext = (update: TelegramUpdate) => {
        if (!latest) {
            return true;
        }
        if ((!latest.message && !latest.edited_message) || (!update.message && !update.edited_message)) {
            return false;
        }

        const latestMessage = latest.message || latest.edited_message;
        const updateMessage = update.message || update.edited_message;

        if (latestMessage && updateMessage) {
            return latestMessage.message_id > updateMessage.message_id;
        } else {
            return false;
        }
    };

    const transformFn = pipe(
        filter(messageIsDefined),
        curry(_sortInAcceptableOrder)(_messageInAscendingOrder),
        filter(notOutOfContext)
    );

    return latest ? append(latest, transformFn(updates)) : transformFn(updates);
}

/**
 * Returns an array of relevant Updates that have been sorted.
 * A relevant update passes at least one of the following tests:
 * - contains a bot command. In this case, the latest Update is returned
 * - the latest Update is the last Update.
 * - Updates that are out of context are removed e.g if an edited message is received,
 * state is rewinded back to the state when the original message was received
 * consequently dumping all the updates that came after the original message.
 * @param updates Array of previous Updates
 * @param latest the latest Update received
 */
const transform = (updates: TelegramUpdate[], latest?: TelegramUpdate): TelegramUpdate[] => {
    // helper functions
    if (latest && isACommand(latest)) {
        return [latest];
    } else {
        return _updateQueueTransform(updates, latest);
    }
}

/**
 * Returns a function that takes an array of Updates that contain a Message and sorts them starting with
 * the earliest Update.
 * `Update.message.message_id` is used to determine how early an Update is.
 * There are no guarantees for the result if an Update without a Message object is found in the starting
 * array.
 * @param sortFn A function for sorting Updates
 */
const _sortInAcceptableOrder = (sortFn: (a: TelegramUpdate, b: TelegramUpdate) => number, updates: TelegramUpdate[]) => {
    return sort(sortFn, updates);
}

export {
    transform
}