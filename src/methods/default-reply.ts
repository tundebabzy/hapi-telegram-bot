import axios from "axios";
import { TELEGRAM_ENDPOINT } from "../constants";

export default function (chatId: string | number) {
    const methodName = 'sendMessage';
    return (token: string) => {
        const endpoint = `${TELEGRAM_ENDPOINT}${token}/${methodName}`;
        return axios.post(endpoint, {
            chat_id: chatId,
            text: "I am embarrased that this is happening but I seem to have lost track of our conversation. Please repeat your last message or let's start over with a new command"
        });
    };
};