import axios from "axios";
import { TELEGRAM_ENDPOINT } from "../constants";
import { ConfigOption, ConfigOptionKeyboard, TelegramReplyKeyboardMarkup, TelegramKeyboardButton, TelegramMethodSendMessage } from "../interface";
import { map } from "rambda";

const sendMessage = function (chatId: string | number, opts: ConfigOption) {
    const methodName = 'sendMessage';
    return (token: string) => {
        const endpoint = `${TELEGRAM_ENDPOINT}${token}/${methodName}`;
        const data: TelegramMethodSendMessage = {
            chat_id: chatId,
            text: opts.text
        };
        const transformToKeyboard = (x: ConfigOptionKeyboard): TelegramKeyboardButton => {
            return ({
                text: x.label,
                request_contact: x.request_contact,
                request_location: x.request_location,
                request_poll: x.request_poll
            });
        };
        const markup = (config: ConfigOption): TelegramReplyKeyboardMarkup => {
            if (!config.keyboards) {
                config.keyboards = [];
            }
            return {
                keyboard: [map(transformToKeyboard, config.keyboards)]
            }
        }

        if (opts.keyboards && opts.keyboards.length) {
            data.reply_markup = markup(opts);
        }

        return axios.post(endpoint, data);
    }
};

export {
    sendMessage
}
