interface Command {
    method: (update: TelegramUpdate) => TelegramMethodSendMessage,
    remoteMethod: string,
    route: string
}

interface Config {
    [key: string]: ConfigOption[]
}

interface ConfigOption {
    condition?: string,
    method: string,
    text: string,
    keyboards?: ConfigOptionKeyboard[]
}

interface ConfigOptionKeyboard {
    label: string,
    request_contact: boolean,
    request_location: boolean,
    request_poll: boolean
}

interface Strategy {
    level: number,
    workflow: Command[]
}

interface PluginOptions {
    allowedUpdates?: string[],
    config: Config,
    secretPath?: string,
    token: string
};

interface TelegramAnimation {
    file_id: string,
    file_unique_id: string,
    width: number,
    height: number,
    duration?: number,
    thumb?: TelegramPhotoSize,
    file_name?: string,
    mime_type?: string,
    file_size?: string
}

interface TelegramAudio {
    file_id: string,
    file_unique_id: string,
    duration: number,
    performer?: string,
    title?: string,
    mime_type?: string,
    file_size: number,
    thumb: TelegramPhotoSize
}

interface TelegramCallbackGame {

}

interface TelegramCallbackQuery {
    id: string,
    from: TelegramUser,
    message?: TelegramMessage,
    inline_message_id?: string,
    chat_instance: string,
    data?: string,
    game_short_name?: string
}

interface TelegramChat {
    id: number,
    type: string,
    title?: string,
    username?: string,
    first_name?: string,
    last_name?: string,
    photo?: TelegramChatPhoto,
    description?: string,
    invite_link?: string,
    pinned_message?: TelegramMessage,
    permissions?: TelegramChatPermissions
    slow_mode_delay?: number,
    sticker_set_name?: string,
    can_set_sticker_set?: boolean
}

interface TelegramChatPermissions {
    can_send_messages?: boolean,
    can_send_media_messages?: boolean,
    can_send_polls: boolean,
    can_send_other_messages?: boolean,
    can_add_web_page_previews?: boolean,
    can_change_info?: boolean,
    can_invite_users?: boolean,
    can_pin_messages?: boolean
}

interface TelegramChatPhoto {
    small_file_id: string,
    small_file_unique_id: string,
    big_file_id: string,
    big_file_unique_id: string
}

interface TelegramChosenInlineQuery {
    result_id: string,
    from: TelegramUser,
    location?: TelegramLocation,
    inline_message_id: string,
    query: string
}

interface TelegramContact {
    phone_number: string,
    first_name: string,
    last_name?: string,
    user_id?: number,
    vcard?: string
}

interface TelegramDocument {
    file_id: string,
    file_unique_id: string,
    thumb?: TelegramPhotoSize,
    file_name?: string,
    mime_type?: string,
    file_size?: number
}

interface TelegramForceReply {
    force_reply: boolean,
    selective?: boolean
}

interface TelegramGame {
    title: string,
    description: string,
    photo: TelegramPhotoSize[],
    text?: string,
    text_entities?: TelegramMessageEntity[],
    animation: TelegramAnimation
}

interface TelegramInlineKeyboardButton {
    text: string,
    url?: string,
    login_url?: TelegramLoginUrl,
    callback_data?: string,
    switch_inline_query?: string,
    switch_inline_query_current_chat?: string,
    callback_game?: TelegramCallbackGame,
    pay?: boolean
}

interface TelegramInlineQuery {
    id: string,
    from: TelegramUser,
    location?: TelegramLocation,
    query: string,
    offset: string
}

interface TelegramInvoice {
    title: string,
    description: string,
    start_parameter: string,
    currency: string,
    total_amount: number
}

interface TelegramKeyboardButton {

}

interface TelegramKeyboardMarkup {
    inline_keyboard: TelegramInlineKeyboardButton[]
}

interface TelegramLocation {
    longitude: number,
    latitude: number
}

interface TelegramLoginUrl {
    url: string,
    forward_text?: string,
    bot_username?: string,
    request_write_access?: boolean
}

interface TelegramMaskPosition {
    point: string,
    x_shift: number,
    y_shift: number,
    scale: number
}

interface TelegramMessage {
    message_id: number,
    from?: TelegramUser,
    date: number,
    chat: TelegramChat,
    forward_from?: TelegramUser,
    forward_from_chat?: TelegramChat,
    forward_from_message_id?: number,
    forward_signature?: string,
    forward_sender_name?: string,
    forward_date?: number,
    reply_to_message?: TelegramMessage,
    edit_date?: number,
    media_group_id?: string,
    author_signature?: string,
    text?: string,
    entities?: TelegramMessageEntity[],
    caption_entities?: TelegramMessageEntity,
    audio?: TelegramAudio,
    document?: TelegramDocument,
    animation?: TelegramAnimation,
    game?: TelegramGame,
    photo?: TelegramPhotoSize[],
    sticker?: TelegramSticker,
    video?: TelegramVideo,
    voice?: TelegramVoice,
    video_note?: TelegramVoiceNote,
    caption?: string,
    contact?: TelegramContact,
    location?: TelegramLocation,
    venue?: TelegramVenue,
    poll?: TelegramPoll,
    new_chat_members?: TelegramUser[],
    left_chat_member?: TelegramUser,
    new_chat_title?: string,
    new_chat_photo?: TelegramPhotoSize[],
    delete_chat_photo?: boolean,
    group_chat_created?: boolean,
    supergroup_chat_created?: boolean,
    channel_chat_created?: boolean,
    migrate_to_chat_id?: number,
    migrate_from_chat_id?: number,
    pinned_message?: TelegramMessage,
    invoice?: TelegramInvoice,
    successful_payment?: TelegramSuccessfulPayment
}

interface TelegramMessageEntity {
    type: "bot_command" | "mention" | "hashtag" | "cashtag" | "url" | "email" | "phone_number" | "bold" | "italic" | "underline" | "strikethrough" | "code" | "pre" | "text_link" | "text_mention",
    offset: number,
    length: number,
    url?: string,
    user?: TelegramUser
}

interface TelegramMethodSendMessage {
    chat_id: number | string,
    text: string,
    parse_mode?: string,
    disable_web_page_preview?: boolean,
    disable_notification?: boolean,
    reply_to_message_id?: number,
    reply_markup?: TelegramKeyboardMarkup | TelegramReplyKeyboardMarkup | TelegramReplyKeyboardRemove | TelegramForceReply
}

interface TelegramOrderInfo {
    name?: string,
    phone_number?: string,
    email?: string,
    shipping_address?: TelegramShippingAddress,
    telegram_payment_charge_id: string,
    provider_payment_charge_id: string
}

interface TelegramPhotoSize {
    file_id: string,
    file_unique_id: string,
    width: number,
    height: number,
    file_size?: number
}

interface TelegramPoll {
    id: string,
    question: string,
    options: TelegramPollOption[]
}

interface TelegramPollOption {
    text: string,
    voter_count: number
}

interface TelegramPreCheckoutQuery {
    id: string,
    from: TelegramUser,
    currency: string,
    total_amount: number,
    invoice_payload: string,
    shipping_option_id?: string,
    order_info: TelegramOrderInfo
}

interface TelegramReplyKeyboardMarkup {
    keyboard: TelegramKeyboardButton[][],
    resize_keyboard?: boolean,
    one_time_keyboard?: boolean,
    selective?: boolean
}

interface TelegramReplyKeyboardRemove {
    remove_keyboard: boolean,
    selective?: boolean
}

interface TelegramShippingAddress {
    country_code: string,
    state: string,
    city: string,
    street_line1: string,
    street_line2: string,
    post_code: string
}

interface TelegramShippingQuery {
    id: string,
    from: TelegramUser,
    invoice_payload: string,
    shipping_address: TelegramShippingAddress
}

interface TelegramSticker {
    file_id: string,
    file_unique_id: string,
    width: number,
    height: number,
    is_animated: boolean,
    thumb?: TelegramPhotoSize,
    emoji?: string,
    set_name?: string,
    mask_position?: TelegramMaskPosition,
    file_size?: number
}

interface TelegramSuccessfulPayment {
    currency: string,
    total_amount: number,
    invoice_payload: string,
    shipping_option_id: string,
    order_info: TelegramOrderInfo
}

interface TelegramUpdate {
    update_id: number,
    message?: TelegramMessage,
    edited_message?: TelegramMessage,
    channel_post?: TelegramMessage,
    edited_channel_post?: TelegramMessage,
    inline_query?: TelegramInlineQuery,
    chosen_inline_result?: TelegramChosenInlineQuery,
    callback_query?: TelegramCallbackQuery,
    shipping_query?: TelegramShippingQuery,
    pre_checkout_query?: TelegramPreCheckoutQuery,
    poll?: TelegramPoll
}

interface TelegramUser {
    id: number,
    is_bot: boolean,
    first_name: string,
    last_name?: string,
    username?: string,
    language_code?: string
}

interface TelegramVenue {
    location: TelegramLocation,
    title: string,
    address: string,
    foursquare_id?: string,
    foursquare_type?: string
}

interface TelegramVideo {
    file_id: string,
    file_unique_id: string,
    width: number,
    height: number,
    duration: number,
    thumb?: TelegramPhotoSize,
    mime_type?: string,
    file_size?: number
}

interface TelegramVoice {
    file_id: string,
    file_unique_id: string,
    duration: number,
    mime_type?: string,
    file_size?: number
}

interface TelegramVoiceNote {
    file_id: string,
    file_unique_id: string,
    length: number,
    duration: number,
    thumb?: TelegramPhotoSize,
    file_size?: number
}

export {
    Config,
    ConfigOption,
    ConfigOptionKeyboard,
    PluginOptions,
    TelegramAnimation,
    TelegramAudio,
    TelegramCallbackGame,
    TelegramCallbackQuery,
    TelegramChat,
    TelegramChatPermissions,
    TelegramChatPhoto,
    TelegramChosenInlineQuery,
    TelegramContact,
    TelegramDocument,
    TelegramForceReply,
    TelegramGame,
    TelegramInlineKeyboardButton,
    TelegramInlineQuery,
    TelegramInvoice,
    TelegramKeyboardButton,
    TelegramKeyboardMarkup,
    TelegramLocation,
    TelegramLoginUrl,
    TelegramMaskPosition,
    TelegramMessage,
    TelegramMessageEntity,
    TelegramMethodSendMessage,
    TelegramOrderInfo,
    TelegramPhotoSize,
    TelegramPoll,
    TelegramPollOption,
    TelegramPreCheckoutQuery,
    TelegramReplyKeyboardMarkup,
    TelegramReplyKeyboardRemove,
    TelegramShippingAddress,
    TelegramShippingQuery,
    TelegramSticker,
    TelegramSuccessfulPayment,
    TelegramUpdate,
    TelegramUser,
    TelegramVenue,
    TelegramVideo,
    TelegramVoice,
    TelegramVoiceNote
}