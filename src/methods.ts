import * as methods from './methods/index'
import { ConfigOption } from './interface';

export default new Map([
    ['default-reply', (chatId: string | number, opts: ConfigOption) => methods.defaultReply(chatId)], // the `opts` argument is there just so that the typings work when imported in other modules
    ['sendMessage', (chatId: string | number, opts: ConfigOption) => methods.sendMessage(chatId, opts)]
]);